#pragma once
#include "IDataAccess.h"
#include "sqlite3.h"

class DatabaseAccess : public IDataAccess
{
public:
	bool open();
	void close();
	void clear();

	// album related
	const std::list<Album> getAlbums();
	const std::list<Album> getAlbumsOfUser(const User& user);
	void createAlbum(const Album& album);
	void deleteAlbum(const std::string& albumName, int userId);
	bool doesAlbumExists(const std::string& albumName, int userId);
	Album openAlbum(const std::string& albumName);
	void closeAlbum(Album& pAlbum);
	void printAlbums();

	// picture related
	 void addPictureToAlbumByName(const std::string& albumName, const Picture& picture);
	 void removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName);
	 void tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId);
	 void untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId);

	// user related
	void printUsers();
	User getUser(int userId);
	void createUser(User& user);
	void deleteUser(const User& user);
	bool doesUserExists(int userId);
	const std::list<User> getUsers();

	// user statistics
	int countAlbumsOwnedOfUser(const User& user);
	int countAlbumsTaggedOfUser(const User& user);
	int countTagsOfUser(const User& user);
	float averageTagsPerAlbumOfUser(const User& user);

	// queries
	User getTopTaggedUser();
	Picture getTopTaggedPicture();
	std::list<Picture> getTaggedPicturesOfUser(const User& user);


private:
	std::list<Album> m_albums;
	std::list<User> m_users;
	User* _chosenUser;
	Album* _openedAlbum;
	sqlite3* _dbHandle;


	static int albumCallback(void* data, int argc, char** argv, char** azColName);
	static int pictureCallback(void* data, int argc, char** argv, char** azColName);
	static int userCallback(void* data, int argc, char** argv, char** azColName);
	static int usersCallback(void* data, int argc, char** argv, char** azColName);
	static int tagCallback(void* data, int argc, char** argv, char** azColName);
	static int intCallback(void* data, int argc, char** argv, char** azColName);
	auto getAlbumIfExists(const std::string& albumName);
};