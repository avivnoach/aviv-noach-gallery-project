#include "DatabaseAccess.h"
#include "Album.h"
#include "User.h"
#include "ItemNotFoundException.h"
#include <io.h>
#include <map>
#include <iostream>

bool DatabaseAccess::open()
{
	sqlite3* db;
	std::string dbName = "galleryDB.sqlite";
	int doesFileExist = _access(dbName.c_str(), 0);
	if (doesFileExist == 0) // if the file exists
	{
		int res = sqlite3_open(dbName.c_str(), &db);
		if (res != SQLITE_OK) // if the opening was successful
		{
			std::cout << "Error opening Database!" << std::endl;
			return false;
		}
		this->_dbHandle = db;

		std::list<Album> albums;
		char* errMassage;
		std::string sqlStatement = ("SELECT * FROM albums;");
		res = sqlite3_exec(_dbHandle, sqlStatement.c_str(), &albumCallback, &albums, &errMassage); // loading albums without pictures
		if (res != SQLITE_OK)
		{
			std::cout << "Error loading albums!" << std::endl;
			return false;
		}
		for (auto& album : albums)
		{
			sqlStatement = ("SELECT PICTURES.ID, PICTURES.NAME, PICTURES.LOCATION, PICTURES.CREATION_DATE FROM PICTURES "
				"INNER JOIN ALBUMS ON ALBUM_ID = ALBUMS.ID "
				"WHERE ALBUMS.NAME = \"" + album.getName() + "\";");
			res = sqlite3_exec(_dbHandle, sqlStatement.c_str(), &pictureCallback, &album, &errMassage); // loading pictures for each album
			if (res != SQLITE_OK)
			{
				std::cout << "Error loading pictures to albums!" << std::endl;
				return false;
			}
		}
		for (auto& album : albums)
		{
			for (auto& picture : album.getPictures())
			{
				sqlStatement = ("SELECT USERS.ID, USERS.NAME FROM TAGS "
					"INNER JOIN USERS ON TAGS.USER_ID = USERS.ID "
					"WHERE PICTURE_ID = " + std::to_string(picture.getId()) + ";");
				res = sqlite3_exec(_dbHandle, sqlStatement.c_str(), &tagCallback, &picture, &errMassage); // loading tags for each picture
				if (res != SQLITE_OK)
				{
					std::cout << "Error loading tags to pictures!" << std::endl;
					return false;
				}
			}
		}
		this->m_albums = albums;

		std::list<User> users;
		sqlStatement = ("SELECT * FROM USERS;");
		res = sqlite3_exec(_dbHandle, sqlStatement.c_str(), usersCallback, &users, &errMassage);
		if (res != SQLITE_OK)
		{
			std::cout << "Error getting User from Database!" << std::endl;
			return false;
		}
		this->m_users = users;
		return true;
	}
	return false;
}

void DatabaseAccess::close()
{
	sqlite3_close(this->_dbHandle);
	clear();
}

void DatabaseAccess::clear()
{
	m_albums.clear();
	m_users.clear();
}

auto DatabaseAccess::getAlbumIfExists(const std::string& albumName)
{
	auto result = std::find_if(std::begin(m_albums), std::end(m_albums), [&](auto& album) { return album.getName() == albumName; });

	if (result == std::end(m_albums)) {
		throw ItemNotFoundException("Album not exists: ", albumName);
	}
	return result;
}

std::list<Album> const DatabaseAccess::getAlbums()
{
	return m_albums;
}

const std::list<Album> DatabaseAccess::getAlbumsOfUser(const User& user)
{
	std::list<Album> albums;
	for (auto& it : m_albums)
	{
		if (it.getOwnerId() == user.getId())
			albums.push_back(it);
	}
	return albums;
}

void DatabaseAccess::createAlbum(const Album& album)
{
	char* errMassage;
	std::string sqlStatement = ("INSERT INTO ALBUMS (NAME, USER_ID, CREATION_DATE) VALUES (\"" + album.getName() + "\"," + std::to_string(album.getOwnerId()) + ",\"" + album.getCreationDate() + "\");");
	int res = sqlite3_exec(_dbHandle, sqlStatement.c_str(), nullptr, nullptr, &errMassage);
	if (res != SQLITE_OK)
	{
		std::cout << "Error inserting album to Database!" << std::endl;
	}
	m_albums.push_back(album);
}

int DatabaseAccess::albumCallback(void* data, int argc, char** argv, char** azColName)
{
	std::list<Album>* albums = (std::list<Album>*)data;
	Album tempAlbum;
	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "NAME")
		{
			tempAlbum.setName(argv[i]);
		}
		else if (std::string(azColName[i]) == "USER_ID")
		{
			tempAlbum.setOwner(std::stoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "CREATION_DATE")
		{
			tempAlbum.setCreationDate(argv[i]);
			(*albums).push_back(tempAlbum);
		}
	}
	return 0;
}


int DatabaseAccess::pictureCallback(void* data, int argc, char** argv, char** azColName)
{
	Album* album = (Album*)data;
	Picture tempPic;
	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "ID")
		{
			tempPic.setId(std::stoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NAME")
		{
			tempPic.setName(argv[i]);
		}
		else if (std::string(azColName[i]) == "LOCATION")
		{
			tempPic.setPath(argv[i]);
		}
		else if (std::string(azColName[i]) == "CREATION_DATE") // the last colomn, after this set the picture is ready to add
		{
			tempPic.setCreationDate(argv[i]);
			album->addPicture(tempPic);
		}
	}
	return 0;
}

int DatabaseAccess::userCallback(void* data, int argc, char** argv, char** azColName)
{
	User* user = (User*)data;
	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "ID")
		{
			(*user).setId(std::atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NAME")
		{
			(*user).setName(argv[i]);
		}
	}
	return 0;
}

int DatabaseAccess::usersCallback(void* data, int argc, char** argv, char** azColName)
{
	std::list<User>* users = (std::list<User>*)data;
	for (int i = 0; i < argc; i++)
	{
		User user;
		if (std::string(azColName[i]) == "ID")
		{
			user.setId(std::atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NAME")
		{
			user.setName(argv[i]);
			(*users).push_back(user);
		}
	}
	return 0;
}


int DatabaseAccess::tagCallback(void* data, int argc, char** argv, char** azColName)
{
	Picture* pic = (Picture*)data;
	for (int i = 0; i < argc; i++)
	{
		User user;
		if (std::string(azColName[i]) == "ID")
		{
			user.setId(std::stoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NAME")
		{
			{
				user.setName(argv[i]);
				(*pic).tagUser(user);
			}
		}
		return 0;
	}
}


void DatabaseAccess::deleteAlbum(const std::string& albumName, int userId)
{
	std::string sqlStatement = ("DELETE FROM ALBUMS "
								"WHERE NAME = \"" + albumName + "\"" + "AND USER_ID = \"" + std::to_string(userId) + "\";");
	char* errMassage;
	int res = sqlite3_exec(_dbHandle, sqlStatement.c_str(), nullptr, nullptr, &errMassage);
	if (res != SQLITE_OK)
	{
		std::cout << "Error removing user!" << std::endl;
	}
	for (auto iter = m_albums.begin(); iter != m_albums.end(); iter++) {
		if (iter->getName() == albumName && iter->getOwnerId() == userId) {
			iter = m_albums.erase(iter);
			return;
		}
	}
}

bool DatabaseAccess::doesAlbumExists(const std::string& albumName, int userId)
{
	for (const auto album : m_albums)
		if (album.getName() == albumName && album.getOwnerId() == userId)
			return true;
	return false;
}

Album DatabaseAccess::openAlbum(const std::string& albumName)
{
	std::list<Album> albums = this->m_albums;
	Album chosenAlbum;
	for (auto it : m_albums)
	{
		if (it.getName() == albumName)
		{
			chosenAlbum =it;
			_openedAlbum = &it;
		}
	}
	albums.clear();
	return chosenAlbum;
}

void DatabaseAccess::closeAlbum(Album& pAlbum)
{
	if (this->_openedAlbum != nullptr)
	{
		this->_openedAlbum = nullptr;
	}
	else
	{
		std::cout << "There is no opened album!" << std::endl;
	}
}

void DatabaseAccess::printAlbums()
{
	std::list<Album> albums = this->m_albums;
	std::cout << "ALBUMS:" << std::endl;
	for (const auto it : m_albums)
	{
		std::cout << "ALBUM NAME: " << it.getName() << std::endl;
		std::cout << "CREATION DATE: " << it.getCreationDate() << std::endl;
		std::cout << "OWNER ID: " << it.getOwnerId() << std::endl;
	}
	albums.clear();
}

void DatabaseAccess::addPictureToAlbumByName(const std::string& albumName, const Picture& picture)
{
	char* errMassage;
	int albumId;
	std::string sqlStatement = ("SELECT ID FROM ALBUMS WHERE NAME = \"" + albumName + "\";");
	int res = sqlite3_exec(_dbHandle, sqlStatement.c_str(), intCallback, &albumId, &errMassage);
	sqlStatement = ("INSERT INTO PICTURES (ID, NAME, LOCATION, CREATION_DATE, ALBUM_ID) "
					"VALUES (" + std::to_string(picture.getId()) + ",\"" + picture.getName() 
					+ "\",\"" + picture.getPath() + "\",\"" + picture.getCreationDate() + "\",\"" 
					+ std::to_string(albumId) + "\");");
	res = sqlite3_exec(_dbHandle, sqlStatement.c_str(), nullptr, nullptr, &errMassage);
	if (res != SQLITE_OK)
	{
		std::cout << "Error, Picture isertion to Database failed!" << std::endl;
		return;
	}

	auto result = getAlbumIfExists(albumName);

	(*result).addPicture(picture);
	return;
}

void DatabaseAccess::removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName)
{
	char* errMassage;
	int albumId;
	std::string sqlStatement = ("SELECT ID FROM ALBUMS WHERE NAME = \"" + albumName + "\";");
	int res = sqlite3_exec(_dbHandle, sqlStatement.c_str(), intCallback, &albumId, &errMassage);
	sqlStatement = ("DELETE FROM PICTURES WHERE NAME = \"" + pictureName + "\" AND ALBUM_ID = " + std::to_string(albumId) + ";");
	res = sqlite3_exec(_dbHandle, sqlStatement.c_str(), nullptr, nullptr, &errMassage);
	if (res != SQLITE_OK)
	{
		std::cout << "Error, Picture removal from Database failed!" << std::endl;
		return;
	}

	auto result = getAlbumIfExists(albumName);
	(*result).removePicture(pictureName);
}

void DatabaseAccess::tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	char* errMassage;
	int pictureId;
	std::string sqlStatement = ("SELECT ID FROM PICTURES "
								"INNER JOIN ALBUMS ON PICTURES.ALBUM_ID = ALBUMS.ID "
								"WHERE PICTURES.NAME = \"" + pictureName + "\" AND ALBUMS.NAME = \"" + albumName + "\";");
	int res = sqlite3_exec(_dbHandle, sqlStatement.c_str(), intCallback, &pictureId, &errMassage);
	sqlStatement = ("INSERT INTO TAGS (PICTURE_ID, USER_ID) VALUES (" + std::to_string(pictureId) + "," + std::to_string(userId) + ");");
	res = sqlite3_exec(_dbHandle, sqlStatement.c_str(), nullptr, nullptr, &errMassage);
	if (res != SQLITE_OK)
	{
		std::cout << "Error, tag insertion to Database failed!" << std::endl;
	}
	
	auto result = getAlbumIfExists(albumName);
	(*result).tagUserInPicture(userId, pictureName);
}

void DatabaseAccess::untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	char* errMassage;
	int pictureId;
	std::string sqlStatement = ("SELECT ID FROM PICTURES "
								"INNER JOIN ALBUMS ON PICTURES.ALBUM_ID = ALBUMS.ID "
								"WHERE PICTURES.NAME = \"" + pictureName + "\" AND ALBUMS.NAME = \"" + albumName + "\";");
	int res = sqlite3_exec(_dbHandle, sqlStatement.c_str(), intCallback, &pictureId, &errMassage);
	sqlStatement = ("DELETE FROM TAGS WHERE PICTURE_ID = " + std::to_string(pictureId) + " AND USER_ID = " + std::to_string(userId) + ";");
	res = sqlite3_exec(_dbHandle, sqlStatement.c_str(), nullptr, nullptr, &errMassage);
	if (res != SQLITE_OK)
	{
		std::cout << "Error, tag removal from Database failed!" << std::endl;
		return;
	}

	auto result = getAlbumIfExists(albumName);
	(*result).untagUserInPicture(userId, pictureName);
}


void DatabaseAccess::printUsers()
{
	std::list<User> users = getUsers();
	std::cout << "USERS:" << std::endl;
	for (const auto& it : m_users)
	{
		std::cout << "ID: " << it.getId() << " NAME: " << it.getName() << std::endl;
	}
}

User DatabaseAccess::getUser(int userId)
{
	if (doesUserExists(userId))
	{
		for (const auto& user : m_users)
		{
			if (user.getId() == userId)
				return user;
		}
	}
	throw ItemNotFoundException("User", userId);
}

void DatabaseAccess::createUser(User& user)
{
	char* errMassage;
	std::string sqlStatement = ("INSERT INTO USERS (ID, NAME) VALUES (" + std::to_string(user.getId()) + ",\"" + user.getName() + "\");");
	int res = sqlite3_exec(_dbHandle, sqlStatement.c_str(), nullptr, nullptr, &errMassage);
	if (res != SQLITE_OK)
	{
		std::cout << "Error, User insertion to Database failed!" << std::endl;
		return;
	}
	m_users.push_back(user);
}

void DatabaseAccess::deleteUser(const User& user)
{
	if (doesUserExists(user.getId()))
	{
		char* errMassage;
		std::string sqlStatement = ("DELETE FROM USERS WHERE ID = " + std::to_string(user.getId()) + ";");
		int res = sqlite3_exec(_dbHandle, sqlStatement.c_str(), nullptr, nullptr, &errMassage);
		if (res != SQLITE_OK)
		{
			std::cout << "Error, User removal from Database failed!" << std::endl;
			return;
		}
		for (auto iter = m_users.begin() ; iter != m_users.end() ;++iter)
		{
			if (*iter == user)
			{
				iter = m_users.erase(iter);
				return;
			}
		}
	}
}

bool DatabaseAccess::doesUserExists(int userId)
{
	auto iter = m_users.begin(); // auto sets the right iterator
	for (const auto& user : m_users)
	{
		if (user.getId() == userId)
			return true;
	}
	return false;
}

const std::list<User> DatabaseAccess::getUsers()
{
	return m_users;
}

int DatabaseAccess::countAlbumsOwnedOfUser(const User& user)
{
	int albumsNum = 0;
	for (const auto& album : this->m_albums)
	{
		if (album.getOwnerId() == user.getId())
			albumsNum++;
	}
	return albumsNum;
}

int DatabaseAccess::countAlbumsTaggedOfUser(const User& user)
// count in howw many albums the user is tagged
{
	int albumsCount = 0;
	for (const auto& album : this->m_albums) // const so i wont change accidently the values
		// this loop method is similar to "for x in y" on Python
		// https://stackoverflow.com/questions/15176104/c11-range-based-loop-get-item-by-value-or-reference-to-const
	{
		const std::list<Picture>& pics = album.getPictures();
		for (const auto& pic : pics)
		{
			if (pic.isUserTagged(user))
			{
				albumsCount++;
				break;
			}
		}
	}
	return albumsCount;
}

int DatabaseAccess::countTagsOfUser(const User& user)
{
	int tagsCount = 0;
	for (const auto& album : this->m_albums) // const so i wont change accidently the values
		// this loop method is similar to "for x in y" on Python
		// https://stackoverflow.com/questions/15176104/c11-range-based-loop-get-item-by-value-or-reference-to-const
	{
		const std::list<Picture>& pics = album.getPictures();
		for (const auto& pic : pics)
		{
			if (pic.isUserTagged(user))
			{
				tagsCount++;
			}
		}
	}
	return tagsCount;
}

// works only when 1 colomn and row is returned from db
int DatabaseAccess::intCallback(void* data, int argc, char** argv, char** azColName)
{
	int* fromDB = (int*)data;
	if (argc == 0)
	{
		*fromDB = NULL;
	}
	else
	{
		*fromDB = std::atoi(argv[0]);
	}
	return 0;
}

float DatabaseAccess::averageTagsPerAlbumOfUser(const User& user)
{
	int numOfAlbumsTagged = countAlbumsTaggedOfUser(user);
	if (numOfAlbumsTagged == 0)
		return 0.0;
	return (countTagsOfUser(user) / numOfAlbumsTagged);
}

User DatabaseAccess::getTopTaggedUser()
{
	std::map<int, int> userTagsCountMap;

	auto albumsIter = m_albums.begin();
	for (const auto& album : m_albums) {
		for (const auto& picture : album.getPictures()) {

			const std::set<int>& userTags = picture.getUserTags();
			for (const auto& user : userTags) {
				//As map creates default constructed values, 
				//users which we haven't yet encountered will start from 0
				userTagsCountMap[user]++;
			}
		}
	}

	if (userTagsCountMap.size() == 0) {
		throw MyException("There isn't any tagged user.");
	}

	int topTaggedUser = -1;
	int currentMax = -1;
	for (auto entry : userTagsCountMap) {
		if (entry.second < currentMax) {
			continue;
		}

		topTaggedUser = entry.first;
		currentMax = entry.second;
	}

	if (-1 == topTaggedUser) {
		throw MyException("Failed to find most tagged user");
	}

	return getUser(topTaggedUser);
}

Picture DatabaseAccess::getTopTaggedPicture()
{
	int currentMax = -1;
	const Picture* mostTaggedPic = nullptr;
	for (const auto& album : m_albums) {
		for (const Picture& picture : album.getPictures()) {
			int tagsCount = picture.getTagsCount();
			if (tagsCount == 0) {
				continue;
			}

			if (tagsCount <= currentMax) {
				continue;
			}

			mostTaggedPic = &picture;
			currentMax = tagsCount;
		}
	}
	if (nullptr == mostTaggedPic) {
		throw MyException("There isn't any tagged picture.");
	}

	return *mostTaggedPic;
}

std::list<Picture> DatabaseAccess::getTaggedPicturesOfUser(const User& user)
{
	std::list<Picture> pictures;

	for (const auto& album : m_albums) {
		for (const auto& picture : album.getPictures()) {
			if (picture.isUserTagged(user)) {
				pictures.push_back(picture);
			}
		}
	}

	return pictures;
}

